import uvicorn
from fastapi import FastAPI, Request, HTTPException, File
from fastapi.middleware.cors import CORSMiddleware
from BlogManagement import Blog
from TagManagement import Tag
from Helpers import create_initial_users, create_initial_blogs, create_initial_tags, get_tags_by_name
from Utils import authenticate_user, handle_image, get_image_paths
from DatabaseQueries import insert_user, get_blog, get_all_blogs
import WebRequests
import WebResponses


app = FastAPI()

origins = [
    "http://localhost",
    "http://localhost:3000",
    "http://localhost:5500",
    "http://localhost:8050",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get('/')
def home():
    return 'endevel task'


@app.post('/create-new-user/')
def create_new_user(create_new_user_request: WebRequests.CreateNewUserRequest):
    try:
        response = insert_user(username=create_new_user_request.username,
                               password=create_new_user_request.password,
                               admin=create_new_user_request.admin)
        query_ok = True if response == 1 else False
        return 'query ok' if query_ok else 'username already exists'
    except Exception as error:
        raise error


@app.post('/fetch-blog/')
def fetch_blog(fetch_blog_request: WebRequests.FetchBlogRequest):
    try:
        blog = get_blog(fetch_blog_request.blog_id)
        if blog is not None:
            full_image_url, thumb_image_url = get_image_paths(fetch_blog_request.blog_id)
            response = WebResponses.FetchBlogResponse(blog=blog.__dict__,
                                                      full_image=full_image_url,
                                                      thumb_image=thumb_image_url)
        else:
            response = {}
        return response
    except Exception as error:
        raise error


@app.get('/fetch-all-blogs/')
def fetch_all_blogs():
    try:
        blogs = get_all_blogs()
        response = blogs
        return response
    except Exception as error:
        raise HTTPException(500, str(error))


@app.post('/upload-image/')
async def upload_image(blog_id, file: bytes = File(...)):
    try:
        file_saved, thumb_image_created = handle_image(file, blog_id)
        return f'file successfully uploaded: {file_saved} | thumbnail created: {thumb_image_created}'
    except Exception as error:
        raise HTTPException(500, str(error))


# ---------------------------------------------------- stary kod -------------------------------------------------------

@app.post('/create-new-blog/')
async def create_new_blog(create_new_blog_request: WebRequests.CreateNewBlogRequest, request: Request):
    if authenticate_user(user_storage=user_storage.storage, provided_user_token=request.headers['user_token']):
        try:
            tags = get_tags_by_name(tag_storage=tag_storage, tag_names=create_new_blog_request.tags)
            new_blog = Blog(title=create_new_blog_request.title,
                            content=create_new_blog_request.content,
                            tags=tags)
            response = blog_storage.add_blog_to_storage(new_blog=new_blog)
            return response
        except Exception as error:
            raise HTTPException(500, str(error))
    else:
        raise HTTPException(403, 'No permissions!')


@app.post('/edit-blog/')
def edit_blog(edit_blog_request: WebRequests.EditBlogRequest, request: Request):
    if authenticate_user(user_storage=user_storage.storage, provided_user_token=request.headers['user_token']):
        try:
            blog: Blog = blog_storage.storage[edit_blog_request.blog_id]
            response = blog.edit_blog(new_title=edit_blog_request.new_title,
                                      new_content=edit_blog_request.new_content)
            return response
        except Exception as error:
            raise HTTPException(500, str(error))
    else:
        raise HTTPException(403, 'No permissions!')


@app.post('/assign-tags/')
def assign_tags(assign_tags_request: WebRequests.AssignTagsRequest, request: Request):
    if authenticate_user(user_storage=user_storage.storage, provided_user_token=request.headers['user_token']):
        try:
            blog: Blog = blog_storage.storage[assign_tags_request.blog_id]
            response = blog.assign_tags(tags=assign_tags_request.tags, tag_storage=tag_storage.storage)
            return response
        except Exception as error:
            raise HTTPException(500, str(error))
    else:
        raise HTTPException(403, 'No permissions!')


@app.post('/remove-tags/')
def remove_tags(remove_tags_request: WebRequests.RemoveTagsRequest, request: Request):
    if authenticate_user(user_storage=user_storage.storage, provided_user_token=request.headers['user_token']):
        try:
            blog: Blog = blog_storage.storage[remove_tags_request.blog_id]
            response = blog.remove_tags(tags=remove_tags_request.tags)
            return response
        except Exception as error:
            raise HTTPException(500, str(error))
    else:
        raise HTTPException(403, 'No permissions!')


@app.post('/fetch-tag/')
def fetch_tag(fetch_tag_request: WebRequests.FetchTagRequest):
    try:
        response = tag_storage.get_tag(tag_name=fetch_tag_request.tag_name)
        return response
    except Exception as error:
        raise HTTPException(500, str(error))


@app.get('/fetch-all-tags/')
def fetch_all_tags():
    try:
        response = tag_storage.get_all_tags()
        return response
    except Exception as error:
        raise HTTPException(500, str(error))


@app.post('/create-new-tag/')
def create_new_tag(create_new_tag_request: WebRequests.CreateNewTagRequest, request: Request):
    if authenticate_user(user_storage=user_storage.storage, provided_user_token=request.headers['user_token']):
        try:
            new_tag = Tag(name=create_new_tag_request.tag_name)
            response = tag_storage.add_tag_to_storage(new_tag=new_tag)
            return response
        except Exception as error:
            raise HTTPException(500, str(error))
    else:
        raise HTTPException(403, 'No permissions!')

# ----------------------------------------------- /stary kod -----------------------------------------------------------


if __name__ == '__main__':
    init()
    uvicorn.run(app, host="127.0.0.1", port=8050)
