from pydantic import BaseModel


class FetchBlogResponse(BaseModel):
    blog: dict
    full_image: str | None = None
    thumb_image: str | None = None
