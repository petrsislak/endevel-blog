import uuid


class Tag:
    def __init__(self, name):
        self.tag_id = str(uuid.uuid4())
        self.name = name


class TagStorage:
    def __init__(self, tags=None):
        if tags is None:
            tags = {}
        self.storage = tags

    def get_tag(self, tag_name):
        if tag_name in self.storage.keys():
            return self.storage[tag_name]
        return None

    def get_all_tags(self):
        return self.storage

    def add_tag_to_storage(self, new_tag: Tag):
        if new_tag.name not in self.storage.keys():
            self.storage[new_tag.name] = new_tag
        return self.storage
