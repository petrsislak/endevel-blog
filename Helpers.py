from lorem_text import lorem
import random

from BlogManagement import Blog
from TagManagement import Tag, TagStorage
from UserManagement import User


def create_initial_users():
    admin = User(username='admin1', password='1234', admin=True)
    casual_user = User(username='pepik', password='1111', admin=False)
    print('admin-token:', admin.token)
    return {admin.token: admin, casual_user.token: casual_user}


def create_initial_tags():
    output = {}
    tag_names = ['politics', 'sport', 'traveling', 'cars', 'technology', 'family', 'pets', 'city', 'denim', 'living']
    print('tag names:', tag_names)
    for tag_name in tag_names:
        output[tag_name] = Tag(tag_name)
    return output


def create_initial_blogs(tag_storage: dict, count=5, tag_count=3):
    output = {}
    title = lorem.sentence()
    content = lorem.paragraphs(2)
    for _ in range(count):
        tags = []
        while len(tags) < tag_count:
            tag = tag_storage[random.choice(list(tag_storage))]
            if tag not in tags:
                tags.append(tag)
        new_blog = Blog(title=title, content=content, tags=tags)
        output[new_blog.blog_id] = new_blog
    return output


def get_tags_by_name(tag_storage: TagStorage, tag_names):
    output = []
    for tag_name in tag_names:
        tag = tag_storage.get_tag(tag_name)
        if tag is not None:
            output.append(tag)
    return output
