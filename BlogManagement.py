from Utils import get_thumb_image
from fastapi.responses import FileResponse
import uuid
import Config


class Blog:
    def __init__(self, blog_id=None, title='blog-title', content='blog-content', tags=None):
        self.blog_id = str(uuid.uuid4()) if blog_id is None else blog_id
        self.title = title
        self.content = content
        self.tags = [] if tags is None else tags
        print('New blog created. ID:', self.blog_id)

    def edit_blog(self, new_title, new_content):
        if new_title is not None:
            self.title = new_title
        if new_content is not None:
            self.content = new_content
        return self

    def assign_tags(self, tags: list, tag_storage: dict):
        for tag_name in tags:
            if tag_name in tag_storage.keys():
                if tag_storage[tag_name] not in self.tags and len(self.tags) < Config.TAGS_PER_BLOG_LIMIT:
                    self.tags.append(tag_storage[tag_name])
        return self

    def remove_tags(self, tags: list):
        tags_to_remove = []
        for removing_tag in tags:
            for blog_tag_index in range(len(self.tags)):
                if removing_tag == self.tags[blog_tag_index].name:
                    tags_to_remove.append(blog_tag_index)
        for i in reversed(sorted(tags_to_remove)):
            del self.tags[i]
        return self


class BlogStorage:
    def __init__(self, blogs=None):
        if blogs is None:
            blogs = {}
        self.storage = blogs

    def get_blog(self, blog_id):
        return self.storage[blog_id]

    def get_all_blogs(self):
        return self.storage

    def add_blog_to_storage(self, new_blog: Blog):
        self.storage[new_blog.blog_id] = new_blog
        return self.storage
