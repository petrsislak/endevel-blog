import sqlite3
from UserManagement import User
from BlogManagement import Blog


def check_if_exist(table_name, column, value):
    output: int
    database = sqlite3.connect("database.db")
    cursor = database.cursor()
    cursor.execute(f'''SELECT * FROM {table_name}
                       WHERE {column}="{value}"''')
    output = cursor.fetchone()
    database.close()
    return output


def parse_tags(string):
    return string.split('-') if len(string) > 0 else None


def insert_user(username, password, admin):
    output = 0
    if check_if_exist('users', 'username', username) is None:
        database = sqlite3.connect("database.db")
        cursor = database.cursor()
        user_object = User(username=username,
                           password=password,
                           admin=admin)
        user_tuple = (user_object.user_id,
                      user_object.username,
                      user_object.password,
                      user_object.admin,
                      user_object.token)
        cursor.execute('''INSERT INTO users (user_id, username, password, admin, token)
                          VALUES (?, ?, ?, ?, ?)''', user_tuple)
        database.commit()
        output = database.total_changes
        database.close()
    return output


def get_blog(blog_id):
    output = []
    database = sqlite3.connect("database.db")
    cursor = database.cursor()
    cursor.execute(f'''SELECT * FROM blogs
                       WHERE blog_id="{blog_id}"''')
    for row in cursor:
        output.append(row)
    database.close()
    if len(output) > 0:
        blog = Blog(blog_id=output[0][0],
                    title=output[0][1],
                    content=output[0][2],
                    tags=parse_tags(output[0][3]))
        return blog
    else:
        return None


def get_all_blogs():
    output, blogs = [], []
    database = sqlite3.connect("database.db")
    cursor = database.cursor()
    cursor.execute('SELECT * FROM blogs')
    for row in cursor:
        output.append(row)
    database.close()
    for result in output:
        blog = Blog(blog_id=result[0],
                    title=result[1],
                    content=result[2],
                    tags=parse_tags(result[3]))
        blogs.append(blog)
    return blogs if len(blogs) > 0 else None
