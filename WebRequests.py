from pydantic import BaseModel, field_validator

import Config


class CreateNewUserRequest(BaseModel):
    username: str
    password: str
    admin: bool

    @field_validator('username', 'password')
    def min_length(cls, value):
        min_length = Config.USER_CREDENTIALS_MIN_LENGTH
        if len(value) < min_length:
            raise ValueError(f'{value} is too short. Min length is {min_length}')
        else:
            return value

    @field_validator('username')
    def max_length(cls, username):
        max_length = Config.USER_CREDENTIALS_MAX_LENGTH
        if len(username) > max_length:
            raise ValueError(f'Username {username} is too long. Max length is {max_length}')
        else:
            return username


class FetchBlogRequest(BaseModel):
    blog_id: str


class CreateNewBlogRequest(BaseModel):
    title: str
    content: str
    tags: list | None = []

    @field_validator('title', 'content')
    def min_length(cls, value):
        min_length = Config.BLOG_FIELD_MIN_LENGTH
        if len(value) < min_length:
            raise ValueError(f'{value} is too short. Min length is {min_length}')
        else:
            return value

    @field_validator('tags')
    def max_length(cls, list_of_tags):
        max_length = Config.TAGS_PER_BLOG_LIMIT
        if len(list_of_tags) > max_length:
            return list_of_tags[0:max_length]
        else:
            return list_of_tags


class EditBlogRequest(BaseModel):
    blog_id: str
    new_title: str | None = None
    new_content: str | None = None

    @field_validator('new_title', 'new_content')
    def min_length(cls, value):
        min_length = Config.BLOG_FIELD_MIN_LENGTH
        if len(value) < min_length:
            raise ValueError(f'{value} is too short. Min length is {min_length}')
        else:
            return value


class AssignTagsRequest(BaseModel):
    blog_id: str
    tags: list

    @field_validator('tags')
    def max_length(cls, list_of_tags):
        max_length = Config.TAGS_PER_BLOG_LIMIT
        if len(list_of_tags) > max_length:
            return list_of_tags[0:max_length]
        else:
            return list_of_tags


class RemoveTagsRequest(BaseModel):
    blog_id: str
    tags: list


class FetchTagRequest(BaseModel):
    tag_name: str


class CreateNewTagRequest(BaseModel):
    tag_name: str

    @field_validator('tag_name')
    def name_length(cls, name):
        min_length = Config.TAG_NAME_MIN_LENGTH
        max_length = Config.TAG_NAME_MAX_LENGTH
        if len(name) < min_length:
            raise ValueError(f'Name {name} is too short. Min length is {min_length}')
        elif len(name) > max_length:
            raise ValueError(f'Name {name} is too long. Max length is {max_length}')
        else:
            return name
