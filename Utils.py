from PIL import Image
import os
import Config


def handle_image(file, file_name):
    with open(f'.\\img\\{file_name}.jpg', 'wb') as image:
        image.write(file)
        image.close()
    image_saved = os.path.isfile(f'.\\img\\{file_name}.jpg')
    if image_saved:
        image = Image.open(f'.\\img\\{file_name}.jpg')
        image.thumbnail((Config.THUMBNAIL_WIDTH, Config.THUMBNAIL_HEIGHT))
        image.save(f'.\\img\\{file_name}_thumb.jpg')
    thumb_image_created = os.path.isfile(f'.\\img\\{file_name}_thumb.jpg')
    return image_saved, thumb_image_created


def get_thumb_image(image_name):
    image = Image.open(f'.\\img\\{image_name}.jpg')
    return image.thumbnail((Config.THUMBNAIL_WIDTH, Config.THUMBNAIL_HEIGHT))


def get_image_paths(image_name):
    full_image, thumb_image = None, None
    if os.path.isfile(f'.\\img\\{image_name}.jpg'):
        full_image = f'{Config.BASE_URL}/img/{image_name}.jpg'
    if os.path.isfile(f'.\\img\\{image_name}_thumb.jpg'):
        thumb_image = f'{Config.BASE_URL}/img/{image_name}_thumb.jpg'
    return full_image, thumb_image


def authenticate_user(user_storage: dict, provided_user_token: str):
    if provided_user_token in user_storage.keys():
        return user_storage[provided_user_token].admin
    else:
        return False
