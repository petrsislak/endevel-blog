import uuid
from nanoid import generate


class User:
    def __init__(self, username, password, admin=False):
        self.user_id = str(uuid.uuid4())
        self.username = username
        self.password = password
        self.admin = admin
        self.token = generate(size=10)


class UserStorage:
    def __init__(self, users=None):
        if users is None:
            users = {}
        self.storage = users

    def get_user(self, user_id):
        return self.storage[user_id]

    def get_all_users(self):
        return self.storage

    def add_user_to_storage(self, new_user: User):
        self.storage[new_user.user_id] = new_user
        return self.storage
    